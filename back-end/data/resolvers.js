import fetch from 'node-fetch';

const stackexchangeResolver = {
    find(args){
        let limit = args.limit || 10;
        let score = args.score || 5;
        let tag = args.tag;
        let sort = args.sort || 'activity';
        let url = `http://api.stackexchange.com/2.2/questions?order=desc&min=${score}&sort=${sort}&tagged=${tag}&pagesize=${limit}&site=stackoverflow`;
        return fetch(url)
        .then(res=> res.json())
        .then(res => {
            return res.items;
        })

    }    
}

const resolvers = {
    Query: {
      question(_, args) {
          return stackexchangeResolver.find(args);
      }
    }
  };
  
  export default resolvers;