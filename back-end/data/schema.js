import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
//import mocks from './mocks';
import resolvers from './resolvers';

const typeDefs = `
type Query {
  question(tag: String!, limit: Int, score: Int, sort: String): [Question]
}

type Question {
  tags: [String]
  score: Int
  title: String
  link: String
  question_id: Int
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

//addMockFunctionsToSchema({ schema, mocks });

export default schema;
