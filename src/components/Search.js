import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql, Query } from 'react-apollo';
import ShowsResult from './ShowsResult'


const questionQuery = gql`
query($tagged: String!, $limit: Int, $score: Int, $sort: String){
  question(tag: $tagged, limit: $limit, score: $score, sort: $sort) {
    tags
    score
    title
    link
    question_id
  }
}
`;

const ShowsResultQuery = graphql(questionQuery, {
    options: ({ tagged, limit, score, sort }) => ({
        variables: {
            tagged: tagged,
            limit: limit,
            score: score,
            sort: sort,
        }
    }),
})(ShowsResult);

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            score: '8',
            sort: 'votes',
            limit: '10',
            tagged: 'Javascript'
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

    }

    handleSelectChange(event) {
        this.setState({ sort: event.target.value });
    }

    render() {
        return (
            <div className="container">
                <h1 className="title-page">StackOverflow</h1>
                <form className="form" >
                    <h2>Buscar na API</h2>

                    <div className="form__input">
                        <label>Tag</label>
                        <input
                            name="tagged"
                            type="text"
                            value={this.state.tagged}
                            // onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form__input">
                        <label>Limit</label>
                        <input
                            name="limit"
                            type="number"
                            value={this.state.limit}
                            onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form__input">
                        <label>Score</label>
                        <input
                            name="score"
                            type="number"
                            value={this.state.score}
                            onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form__input">
                        <label>Sort</label>
                        <select name="sort" value={this.state.value} onChange={this.handleSelectChange}>
                            <option value="votes">Votes</option>
                            <option value="activity">Activity</option>
                            <option value="creation">Creation</option>
                            <option value="hot">Hot</option>
                            <option value="week">Week</option>
                            <option value="month">Month</option>
                        </select>
                    </div>

                    <div className="form_button">
                        <a href="javascript:void(0)" className="upvotes" onClick={(event) => { this.handleInputChange; this.handleSelectChange; }} > Buscar</a>
                    </div>



                </form>

                <ShowsResultQuery
                    tagged={this.state.tagged}
                    limit={this.state.limit}
                    score={this.state.score}
                    sort={this.state.sort}
                />
            </div>

        );
    }
}

export default Search;