import React, { Component } from 'react';
import logo from './../assets/images/logo.jpg';


class Header extends React.Component {
    render() {
        return (
            <header className="header" >
                <div className="container" >
                    <img src={logo} alt="" />
                </div>
            </header>
        );
    }
}

export default Header;