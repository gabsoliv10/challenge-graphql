import React, { Component } from 'react';

class ShowsResult extends Component {
    render() {
        const { data: { loading, error, question } } = this.props;

        if (loading) {
            return <div className="loader"></div>;
        }
        if (error) {
            return <h4>{error.message}</h4>;
        }

        if (question === null) {
            return <div className="questions__null">Ops, sua busca não foi encontrada. Tente novamente!</div>
        }
        return (
            <div className="questions">
                <div className="row">
                    {question && question.map(question =>
                        <div className="col-sm-4">
                            <li className="questions__box" key={question.question_id}>
                                <a href={question.link}>
                                    <span className="questions__id">{question.question_id}</span>
                                    <p className="questions__title" >
                                    {question.title}
                                    </p>                                   
                                </a>
                            </li>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default ShowsResult;