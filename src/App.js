import React, { Component } from 'react';
import Search from './components/Search'
import Header from './components/Header'
import Footer from './components/Footer'
import {ApolloClient} from 'apollo-client'
import { ApolloProvider } from 'react-apollo';
import {HttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory';
import './assets/stylesheets/css/style.css';
import gql from 'graphql-tag';

export const client = new ApolloClient({
  link: new HttpLink({
    uri: 'http://localhost:4000/graphql'
  }),
  cache: new InMemoryCache()
})

client.query({ query: gql`{ question(tag: "php") { title } }` }).then(console.log);
class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Search />
        <Footer />
      </div>
    );
  }
}

const ApolloApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

export default ApolloApp;

